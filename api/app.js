var express = require('express')
var cors = require('cors')
var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser')
var session = require('express-session')

require("./database/mango")()

var LocalStorage = require('node-localstorage').LocalStorage;

var localStorage = new LocalStorage('./scratch')
var app = express()
var port = process.env.PORT || 4000

app.set('trust proxy', 1)

app.use(cors({ origin: 'http://localhost:4001' }))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))




app.use(cookieParser("keyboard"));

app.use(session({ secret: 'Hithisnotmykey', saveUninitialized: true, resave: true }));

var localsession, localsessions = [];

var newproject=require("./router/projectnewrouter")

app.use("/",newproject)

// var name = "Mathesh"
// app.get("/", (req, res) => {

//     localStorage.setItem('myFirstKey', 'myFirstValue');
//     console.log(localStorage.getItem('myFirstKey'));
//     return res.send({ name: name })

// })

// app.post("/senddata", (req, res) => {
//     localsession = req.session
//     localsession.name = "Mathesh"
//     localsession.email = "matheshsureshofficial@gmail.com"
//     localsessions["name"] = req.body.name
//     localsessions["email"] = req.body.email
//     return res.send({ name: localsessions.name, email: localsessions.email })

// })

// app.get("/viewdata", (req, res) => {
//     console.log(localStorage.getItem('myFirstKey'), "local");
//     localsession = req.session
//     console.log(localsession.email)
//     return res.send({ name: localsessions.name, email: localsessions.email })

// })

app.listen(port, () => { console.log(`App Runiing On http://localhost:${port}`) })