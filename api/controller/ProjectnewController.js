var express = require('express')
var ObjectId = require('mongodb').ObjectID;
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/jobapplication";
module.exports.getproject = (req, res) => {
    MongoClient.connect(url, { useUnifiedTopology: true }, function (err, db) {
        if (err) throw err;
        var dbname = db.db("jobapplication");
        dbname.collection("applications").find({}).sort({name:1}).toArray(function (err, result) {
            if (err) throw err;
            if (result.length != 0) {
                res.send(result)
                return db.close();
            } else {
                res.send("No Data Found")
            }
        });
    })
}
module.exports.senddata = (req, res) => {
    console.log(req.body)

    MongoClient.connect(url, { useUnifiedTopology: true }, function (err, db) {
        if (err) throw err;
        var dbname = db.db("jobapplication");
        dbname.collection('applications').insertOne({
            name: req.body.name,
            email: req.body.email,
            number: req.body.number,
            qualification: req.body.qualification,
            category: req.body.category,
            approved: false
        }).then(function (result) {
            return res.send(result.ops)
        })
    });

}

module.exports.update = (req, res) => {    
    MongoClient.connect(url, { useUnifiedTopology: true }, function (err, db) {
        if (err) throw err;
        var dbname = db.db("jobapplication");
        dbname.collection("applications").updateOne(
            { _id: ObjectId(req.body.id) },
            {
                $set: {
                    approved: req.body.value                    
                }
            }, function (err, results) {
                if (err) throw console.log(err, "err");
                console.log("1 document updated");
                if (results.result.nModified != 0) {
                    res.send("Data Updated")
                    return db.close();
                } else {
                    res.send("Data not Updated")
                    return db.close();
                }
            });
    });
}

module.exports.createjob=(req,res)=>{
    console.log(req.body)
    MongoClient.connect(url, { useUnifiedTopology: true }, function (err, db) {
        if (err) throw err;
        var dbname = db.db("jobapplication");
        dbname.collection('jobs').insertOne({
            jobname: req.body.jobname,          
        }).then(function (result) {
            return res.send(result.ops)
        })
    });    
}

module.exports.jobget=(req,res)=>{
    MongoClient.connect(url, { useUnifiedTopology: true }, function (err, db) {
        if (err) throw err;
        var dbname = db.db("jobapplication");
        dbname.collection("jobs").find({}).sort({name:1}).toArray(function (err, result) {
            if (err) throw err;
            if (result.length != 0) {
                res.send(result)
                return db.close();
            } else {
                res.send("No Data Found")
            }
        });
    })
}

module.exports.jobdelete=(req,res)=>{
    MongoClient.connect(url, { useUnifiedTopology: true }, function (err, db) {
        if (err) throw err;
        var dbname = db.db("jobapplication");
        dbname.collection("jobs").deleteOne(
            { _id: ObjectId(req.body.id) },
            {             
            }, function (err, results) {
                if (err) throw console.log(err, "err");
                console.log(results.result.ok)
                if (results.result.ok != 0) {
                    res.send("Data Updated")
                    return db.close();
                } else {
                    res.send("Data not Updated")
                    return db.close();
                }
            });
    });
}