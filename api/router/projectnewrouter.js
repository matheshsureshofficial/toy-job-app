var express=require('express')
var router=express.Router()

var ProjectnewController=require("../controller/ProjectnewController")

router.get("/",ProjectnewController.getproject)
router.post("/senddata",ProjectnewController.senddata)
router.post("/update",ProjectnewController.update)
router.post("/createjob",ProjectnewController.createjob)
router.get("/jobget",ProjectnewController.jobget)
router.post("/jobdelete",ProjectnewController.jobdelete)
module.exports = router;