import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import JobApply from '../views/JobApply.vue'
import ApplyForm from '../views/ApplyForm.vue'
import JobCreate from '../views/AdminJobCreate.vue'
import JobApplied from '../views/AdminJobApplied.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/jobapply/:post',
    name: 'JobApply',
    component: JobApply
  }
  ,
  {
    path: '/applyform/:postapply',
    name: 'ApplyForm',
    component: ApplyForm
  },
  {
    path: '/jobcreate',
    name: 'JobCreate',
    component: JobCreate
  },
  {
    path: '/jobapplied/:applied',
    name: 'JobApplied',
    component: JobApplied
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
